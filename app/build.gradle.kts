import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinAndroidExtensions)
    id(BuildPlugins.kotlinKapt)
    id(BuildPlugins.NavigationSafeargs)
}

var keystorePropertiesFile = File(rootDir, "keystore.properties")
val keystoreProperties = Properties()
keystoreProperties.load(keystorePropertiesFile.inputStream())

androidExtensions {
    isExperimental = true
    defaultCacheImplementation = org.jetbrains.kotlin.gradle.internal.CacheImplementation.SPARSE_ARRAY
}

android {

    signingConfigs {
        create("release") {
            storeFile = File(keystorePropertiesFile.parentFile, keystoreProperties["storeFile"] as String)
            storePassword = keystoreProperties["storePassword"] as String
            keyAlias = keystoreProperties["keyAlias"] as String
            keyPassword = keystoreProperties["keyPassword"] as String
        }
    }

    compileSdkVersion(AndroidSdk.compile)
    defaultConfig {
        applicationId = "me.meikiem.heroworkoutchallenge"
        minSdkVersion(AndroidSdk.min)
        targetSdkVersion(AndroidSdk.target)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField("String", "API_URL", "\"\"")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    viewBinding.isEnabled = true

    dataBinding.isEnabled = true
}

// config JVM target to 1.8 for kotlin compilation tasks
tasks.withType < org.jetbrains.kotlin.gradle.tasks.KotlinCompile > ().configureEach {
    kotlinOptions.jvmTarget = "1.8"
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libraries.kotlinStdLib)
    implementation(Libraries.appCompat)
    implementation(Libraries.constraintLayout)
    implementation(Libraries.ktxCore)
    implementation(Libraries.roomRunTime)
    implementation(Libraries.legacy)
    kapt(Libraries.roomCompiler)
    implementation(Libraries.navigationFragment)
    implementation(Libraries.navigationUI)
    implementation(Libraries.navigationFragmentKtx)
    implementation(Libraries.navigationFragmentUiKtx)
    implementation(Libraries.lifecycleExtensions)
    implementation(Libraries.lifecycleViewModel)
    kapt(Libraries.lifecycleCompiler)
    implementation(Libraries.glide)
    implementation(Libraries.preference)
    implementation(Libraries.dagger)
    kapt(Libraries.daggerCompiler)
    implementation(Libraries.daggerAndroid)
    implementation(Libraries.daggerAndroidSupport)
    implementation(Libraries.materialComponent)
    kapt(Libraries.daggerAndroidProcessor)
}