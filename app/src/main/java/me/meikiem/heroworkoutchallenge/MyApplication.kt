package me.meikiem.heroworkoutchallenge

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import me.meikiem.heroworkoutchallenge.di.DaggerAppComponent

class MyApplication : DaggerApplication() {


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }


}