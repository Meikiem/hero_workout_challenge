package me.meikiem.heroworkoutchallenge.data.Provider

import me.meikiem.heroworkoutchallenge.internal.Theme

interface ThemeProvider {
    fun getTheme(): Theme
    fun setTheme(theme: Theme)
}