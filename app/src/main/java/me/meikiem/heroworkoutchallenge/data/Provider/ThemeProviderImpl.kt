package me.meikiem.heroworkoutchallenge.data.Provider

import android.content.Context
import me.meikiem.heroworkoutchallenge.internal.Theme
import javax.inject.Inject

const val THEME = "THEME"

class ThemeProviderImpl @Inject constructor(context: Context) : PreferenceProvider(context), ThemeProvider {
    override fun getTheme(): Theme {
        val selectedTheme = preferences.getString(THEME, Theme.DARK.name)
        return Theme.valueOf(selectedTheme!!)
    }

    override fun setTheme(theme: Theme) {
        preferences.edit().putString(THEME, theme.name).apply()
    }
}