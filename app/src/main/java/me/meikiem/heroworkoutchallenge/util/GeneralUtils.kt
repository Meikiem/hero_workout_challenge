package me.meikiem.heroworkoutchallenge.util

import kotlinx.android.synthetic.main.login_fragment.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.isValidEmail() =
        this.isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.isValidPassword(): Boolean {
    val passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%!^&+=])(?=\\S+$).{12,}$"
    return this.isNotEmpty() && Pattern.compile(passwordPattern).matcher(this).matches()
}