package me.meikiem.heroworkoutchallenge.presentation.ui.spash

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.meikiem.heroworkoutchallenge.di.ViewModelKey
import me.meikiem.heroworkoutchallenge.presentation.ui.login.LoginViewModel

@Module
internal abstract class SplashFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun splashViewModel(fragmentViewModel: SplashViewModel): ViewModel
}