package me.meikiem.heroworkoutchallenge.presentation

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.meikiem.heroworkoutchallenge.di.PerActivity
import me.meikiem.heroworkoutchallenge.di.PerFragment
import me.meikiem.heroworkoutchallenge.presentation.ui.login.LoginFragment
import me.meikiem.heroworkoutchallenge.presentation.ui.login.LoginFragmentModule
import me.meikiem.heroworkoutchallenge.presentation.ui.spash.SplashFragment
import me.meikiem.heroworkoutchallenge.presentation.ui.spash.SplashFragmentModule

@Module
abstract class MainActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = [SplashFragmentModule::class])
    internal abstract fun splashFragment(): SplashFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [LoginFragmentModule::class])
    internal abstract fun loginFragment(): LoginFragment

    @Binds
    @PerActivity
    internal abstract fun mainNavigator(mainActivity: MainActivity): MainNavigator


}