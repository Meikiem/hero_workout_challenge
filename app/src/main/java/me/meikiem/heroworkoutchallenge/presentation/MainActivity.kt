package me.meikiem.heroworkoutchallenge.presentation

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import dagger.android.support.DaggerAppCompatActivity
import me.meikiem.heroworkoutchallenge.R
import me.meikiem.heroworkoutchallenge.data.Provider.ThemeProvider
import me.meikiem.heroworkoutchallenge.databinding.ActivityMainBinding
import me.meikiem.heroworkoutchallenge.internal.Theme
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainNavigator {

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var themeProvider: ThemeProvider

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        if(themeProvider.getTheme().name == Theme.LIGHT.name)
            setTheme(R.style.LightLoginTheme)
        else setTheme(R.style.DarkLoginTheme)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        window?.statusBarColor = Color.TRANSPARENT
    }

    override fun onBack() {
        findNavController(R.id.nav_host_main).navigateUp()
    }

    override fun goToLogin() {
        findNavController(R.id.nav_host_main).navigate(R.id.action_splashFragment_to_loginFragment)
    }

    override fun onLightThemeClicked(view: View) {
        themeProvider.setTheme(Theme.LIGHT)
        recreate()
    }

    override fun onDarkThemeClicked(view: View) {
        themeProvider.setTheme(Theme.DARK)
        recreate()
    }


}