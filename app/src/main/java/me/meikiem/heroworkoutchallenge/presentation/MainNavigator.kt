package me.meikiem.heroworkoutchallenge.presentation

import android.view.View

interface MainNavigator {
    fun onBack()
    fun goToLogin()
    fun onLightThemeClicked(view: View)
    fun onDarkThemeClicked(view: View)
}