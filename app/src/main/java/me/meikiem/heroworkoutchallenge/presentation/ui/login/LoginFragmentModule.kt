package me.meikiem.heroworkoutchallenge.presentation.ui.login

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.meikiem.heroworkoutchallenge.di.ViewModelKey

@Module
internal abstract class LoginFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun loginViewModel(fragmentViewModel: LoginViewModel): ViewModel
}