package me.meikiem.heroworkoutchallenge.presentation.ui.spash

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.NavGraph
import androidx.navigation.fragment.findNavController
import dagger.android.support.DaggerFragment
import me.meikiem.heroworkoutchallenge.R
import me.meikiem.heroworkoutchallenge.databinding.LoginFragmentBinding
import me.meikiem.heroworkoutchallenge.databinding.SplashFragmentBinding
import me.meikiem.heroworkoutchallenge.presentation.MainNavigator
import me.meikiem.heroworkoutchallenge.presentation.extension.viewModelProvider
import javax.inject.Inject

class SplashFragment : DaggerFragment() {

    @Inject
    lateinit var navigator: MainNavigator

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var viewModel: SplashViewModel

    private lateinit var binding: SplashFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModelProvider(factory)
        navigator.goToLogin()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SplashFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    private fun initUI() {

    }
}