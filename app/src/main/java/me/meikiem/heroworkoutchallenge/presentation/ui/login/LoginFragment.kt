package me.meikiem.heroworkoutchallenge.presentation.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextWatcher
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputLayout
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.login_fragment.*
import me.meikiem.heroworkoutchallenge.R
import me.meikiem.heroworkoutchallenge.databinding.LoginFragmentBinding
import me.meikiem.heroworkoutchallenge.presentation.MainNavigator
import me.meikiem.heroworkoutchallenge.presentation.extension.viewModelProvider
import me.meikiem.heroworkoutchallenge.util.isValidEmail
import me.meikiem.heroworkoutchallenge.util.isValidPassword
import javax.inject.Inject


class LoginFragment : DaggerFragment() {

    @Inject
    lateinit var navigator: MainNavigator

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var viewModel: LoginViewModel

    private lateinit var binding: LoginFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModelProvider(factory)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = LoginFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loginButton.setOnClickListener {
            if (isEmailValid() && isValidPassword()) {
                val dialogBuilder = AlertDialog.Builder(requireContext())
                dialogBuilder.setMessage(getString(R.string.successful_login))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                            dialog.dismiss()
                        }
                val alert = dialogBuilder.create()
                alert.setTitle(getString(R.string.login))
                alert.show()
            }
        }
        passwordEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if(passwordLayout.endIconMode == TextInputLayout.END_ICON_NONE){
                    passwordLayout.endIconMode = TextInputLayout.END_ICON_PASSWORD_TOGGLE
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    private fun isEmailValid(): Boolean {
        if (!emailEditText.text.toString().isValidEmail()) {
            setEmailError(getString(R.string.email_err_propmt))
            return false
        }
        return true
    }

    private fun isValidPassword(): Boolean {
        if (passwordEditText.text.toString().isEmpty()) {
            setPasswordError(getString(R.string.password_empty_prompt))
        }
        if (!passwordEditText.text.toString().isValidPassword()) {
            setPasswordError(getString(R.string.password_pattern_prompt))
            return false
        }
        return true
    }

    private fun setPasswordError(error: String) {
        passwordLayout.endIconMode = TextInputLayout.END_ICON_NONE
        passwordEditText.error = SpannableString(error).apply {
            val errorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_error)
            setSpan(ImageSpan(errorDrawable!!, ImageSpan.ALIGN_BASELINE), 0, 0, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        }
        requestFocus(passwordLayout)
    }

    private fun setEmailError(error: String) {
        emailEditText.error = SpannableString(error).apply {
            val errorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_error)
            setSpan(ImageSpan(errorDrawable!!, ImageSpan.ALIGN_BASELINE), 0, 0, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        }
        requestFocus(emailLayout)
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


}