package me.meikiem.heroworkoutchallenge.di.data

import dagger.Binds
import dagger.Module
import me.meikiem.heroworkoutchallenge.data.Provider.ThemeProvider
import me.meikiem.heroworkoutchallenge.data.Provider.ThemeProviderImpl

@Module
abstract class ProviderModule {

    @Binds
    abstract fun getThemeProvider(implementation: ThemeProviderImpl): ThemeProvider
}