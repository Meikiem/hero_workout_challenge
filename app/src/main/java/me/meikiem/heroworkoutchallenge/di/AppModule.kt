package me.meikiem.heroworkoutchallenge.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import me.meikiem.heroworkoutchallenge.presentation.MainActivity
import me.meikiem.heroworkoutchallenge.presentation.MainActivityModule

@Module
abstract class AppModule{
    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

    @Binds
    abstract fun bindContext(application: Application): Context

}