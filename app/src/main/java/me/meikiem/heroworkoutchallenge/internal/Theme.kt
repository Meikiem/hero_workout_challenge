package me.meikiem.heroworkoutchallenge.internal

enum class Theme {
    LIGHT, DARK
}